//
//  ProductCellPageObjects.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

class ProductCellPageObjects: ProductCellPageObjectsType {
    
    var app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    var productImage: XCUIElement {
        let productImage = app.images[Constants.AccesibilityIdentifier.productImageProductTableCell]
        return productImage
    }
    
    var productTitleLabel: XCUIElement {
        let productTitle = app.staticTexts[Constants.AccesibilityIdentifier.ProductTitleLableTableCell]
        return productTitle
    }
    
    var prductPriceLable: XCUIElement {
        let prductPriceLabel = app.staticTexts[Constants.AccesibilityIdentifier.productPriceLabelTableCell]
        return prductPriceLabel
    }
    
}
