//
//  ProductCellPageObjectsType.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

protocol ProductCellPageObjectsType {
    
    var productImage: XCUIElement { get }
    var productTitleLabel: XCUIElement { get }
    var prductPriceLable: XCUIElement { get }
}
