//
//  ProductDetailPageObjects.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

class ProductDetailPageObjects: ProductDetailPageObjectsType {
    
    var app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    var productImage: XCUIElement {
        return app.images[Constants.AccesibilityIdentifier.productDetailImageView]
    }
    
    var productPriceLabel: XCUIElement {
        return app.staticTexts[Constants.AccesibilityIdentifier.productDetailPrice]
    }
    
    var productTitleLable: XCUIElement {
        return app.staticTexts[Constants.AccesibilityIdentifier.productDetailTitle]
    }
    
    var productDetailNavigationBar: XCUIElement {
        return app.navigationBars["Details"]
    }
    
    var productDetailbackButton: XCUIElement {
        return app.navigationBars.buttons["Gousto"]
    }
    
    var productDescriptionTextView: XCUIElement {
        return app.textViews[Constants.AccesibilityIdentifier.productDetailDescription]
    }
}
