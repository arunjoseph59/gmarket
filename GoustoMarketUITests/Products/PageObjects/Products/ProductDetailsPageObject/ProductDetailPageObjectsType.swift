//
//  ProductDetailPageObjectsType.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

protocol ProductDetailPageObjectsType {
    
    var productImage: XCUIElement { get }
    var productPriceLabel: XCUIElement { get }
    var productTitleLable: XCUIElement { get }
    var productDetailNavigationBar: XCUIElement { get }
    var productDetailbackButton: XCUIElement { get }
    var productDescriptionTextView: XCUIElement { get }
}
