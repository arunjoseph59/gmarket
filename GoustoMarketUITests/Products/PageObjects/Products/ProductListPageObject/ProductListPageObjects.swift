//
//  ProductListPageObjects.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

class ProductListPageObjects: ProductListPageObjectType {
    
    var app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    var table: XCUIElement {
        return app.tables[Constants.AccesibilityIdentifier.productsTableView]
    }
    
    var search: XCUIElement {
        return app.searchFields["Search"]
    }
    
    var searchClearButton: XCUIElement {
        return self.search.buttons["Clear text"]
    }
    
    func getCell(at index: Int)-> XCUIElement {
       return self.table.cells.element(boundBy: index)
    }
}
