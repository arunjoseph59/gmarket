//
//  ProductLisPageObjectType.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

protocol ProductListPageObjectType {
    
    var table: XCUIElement { get }
    var search: XCUIElement { get }
    var searchClearButton: XCUIElement { get }
    
    func getCell(at index: Int)-> XCUIElement
}
