//
//  ErrorViewPageObjects.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

class ErrorViewPageObjects: ErrorViewPageObjectsType {
    
    private var app: XCUIApplication
    
    init(app: XCUIApplication) {
        self.app = app
    }
    
    var closeButton: XCUIElement {
        let closeButton = app.buttons[Constants.AccesibilityIdentifier.erroViewCloseButton]
        return closeButton
    }
    
    var erroMessageLabel: XCUIElement {
        let errorMessage = app.staticTexts[Constants.AccesibilityIdentifier.erroViEwerrorMessageLabel]
        return errorMessage
    }
    
    var errortext: String {
        let errortext = app.staticTexts[Constants.AccesibilityIdentifier.erroViEwerrorMessageLabel].label
        return errortext
    }
    
    var errorImage: XCUIElement {
        let errorImage = app.images[Constants.AccesibilityIdentifier.erroViewErrorImageView]
        return errorImage
    }
    
    func closeErrorView() {
        let closeButton = app.buttons[Constants.AccesibilityIdentifier.erroViewCloseButton]
        closeButton.tap()
    }
}
