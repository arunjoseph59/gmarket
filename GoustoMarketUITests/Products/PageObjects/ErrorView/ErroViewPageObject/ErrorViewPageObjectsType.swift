//
//  ErrorViewPageObjectsType.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

protocol ErrorViewPageObjectsType {
    var closeButton: XCUIElement { get }
    var erroMessageLabel: XCUIElement { get }
    var errorImage: XCUIElement { get }
    var errortext: String { get }
    
    func closeErrorView()
}
