//
//  Helper.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import XCTest

enum Swipe {
    case up
    case down
    case right
    case left
}


extension XCUIElement {
    
    func swipe(to: Swipe) {
        switch to {
        case .up:
            self.swipeUp()
        case .down:
            self.swipeDown()
        case .left:
            self.swipeLeft()
        case .right:
            self.swipeRight()
        }
    }
}
