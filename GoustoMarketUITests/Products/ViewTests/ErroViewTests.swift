//
//  ErroViewTests.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 06/02/22.
//

import XCTest

class ErroViewTests: XCTestCase {
    
    var app: XCUIApplication!
    var erroViewPageObject: ErrorViewPageObjectsType!
    
    override func setUpWithError() throws {
        self.app = XCUIApplication()
        self.continueAfterFailure = false
        self.erroViewPageObject = ErrorViewPageObjects(app: app)
        self.app.launchArguments.append("UITesting")
    }
    
    override func tearDownWithError() throws {
        self.app = nil
    }
    
    func test_all_UI_elements_are_exists_when_error_occured() {
        self.app.launchArguments.append("ErrorOccured")
        self.app.launch()

        
        XCTAssert(self.erroViewPageObject.closeButton.exists)
        XCTAssert(self.erroViewPageObject.closeButton.exists)
        XCTAssertNotNil(self.erroViewPageObject.erroMessageLabel.exists)
        XCTAssert(!self.erroViewPageObject.errortext.isEmpty)
        XCTAssert(self.erroViewPageObject.errorImage.exists)
        XCTAssert(self.erroViewPageObject.closeButton.isHittable)
    }
}
