//
//  ProductsListSearchAndDetailFlowTest.swift
//  GoustoMarketUITests
//
//  Created by A-10474 on 01/02/22.
//

import XCTest

class ProductsListSearchAndDetailFlowTest: XCTestCase {
    
    var app: XCUIApplication!
    var productListPageObjects: ProductListPageObjectType!
    var productDetailPageObject: ProductDetailPageObjectsType!
    var productCellPageObject: ProductCellPageObjectsType!
    
    override func setUpWithError() throws {
        self.app = XCUIApplication()
        self.productCellPageObject = ProductCellPageObjects(app: self.app)
        self.productDetailPageObject = ProductDetailPageObjects(app: self.app)
        self.productListPageObjects = ProductListPageObjects(app: self.app)
        self.continueAfterFailure = false
        self.app.launchArguments.append("UITesting")
        self.app.launch()
    }
    
    override func tearDownWithError() throws {
        self.productListPageObjects = nil
        self.productDetailPageObject = nil
        self.productCellPageObject = nil
        self.app = nil
    }
    
    func test_app_normal_flow_without_search() {
        
        self.productListPageObjects.table.swipeUp()
        self.productListPageObjects.table.swipeDown()
        
        
        XCTAssert(self.productListPageObjects.table.exists)
        XCTAssert(self.productListPageObjects.getCell(at: 0).exists)
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 7)
        XCTAssert(self.productListPageObjects.search.exists)
        XCTAssert(self.productListPageObjects.getCell(at: 0).isHittable)
        XCTAssert(self.productListPageObjects.getCell(at: 1).isHittable)
        XCTAssert(self.productListPageObjects.getCell(at: 2).isHittable)


        self.productListPageObjects.getCell(at: 0).tap()
        self.productDetailPageObject.productDescriptionTextView.swipeUp()
        self.productDetailPageObject.productDescriptionTextView.swipeDown()


        XCTAssert(self.productDetailPageObject.productTitleLable.exists)
        XCTAssert(self.productDetailPageObject.productPriceLabel.exists)
        XCTAssert(self.productDetailPageObject.productDescriptionTextView.exists)
        XCTAssert(self.productDetailPageObject.productDetailNavigationBar.exists)
        XCTAssert(self.productDetailPageObject.productDetailbackButton.exists)
        XCTAssert(self.productDetailPageObject.productImage.exists)
        XCTAssert(self.productDetailPageObject.productDetailbackButton.isHittable)


        self.productDetailPageObject.productDetailbackButton.tap()


        XCTAssert(self.productListPageObjects.table.exists)
        XCTAssert(self.productListPageObjects.getCell(at: 0).exists)
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 7)
        XCTAssert(self.productListPageObjects.search.exists)
        XCTAssert(self.productListPageObjects.getCell(at: 0).isHittable)
        XCTAssert(self.productListPageObjects.getCell(at: 1).isHittable)
        XCTAssert(self.productListPageObjects.getCell(at: 2).isHittable)

    }
    
    func test_app_normal_flow_with_search() {
        self.productListPageObjects.search.tap()

        
        XCTAssert(self.productListPageObjects.table.exists)
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 7)
        XCTAssert(self.productListPageObjects.search.isHittable)
        
        
        self.productListPageObjects.search.tap()
        self.productListPageObjects.search.typeText("Love")
        
        
        XCTAssert(self.productListPageObjects.table.exists)
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 1)
        
        
        self.productListPageObjects.search.tap()
        self.productListPageObjects.searchClearButton.tap()
        
        
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 7)

        
        self.productListPageObjects.search.tap()
        self.productListPageObjects.search.typeText("Borsao")
        
        
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 1)
        
        
        self.productListPageObjects.getCell(at: 0).tap()
        self.productDetailPageObject.productDescriptionTextView.swipeUp()
        self.productDetailPageObject.productDescriptionTextView.swipeDown()
        
        
        XCTAssert(self.productDetailPageObject.productPriceLabel.exists)
        XCTAssert(self.productDetailPageObject.productDescriptionTextView.exists)
        XCTAssert(self.productDetailPageObject.productImage.exists)
        
        
        XCTAssert(self.productDetailPageObject.productDetailNavigationBar.exists)
        XCTAssert(self.productDetailPageObject.productDetailbackButton.exists)
        
        
        self.productDetailPageObject.productDetailbackButton.tap()
        
        
        XCTAssert(self.productListPageObjects.table.exists)
        XCTAssertEqual(self.productListPageObjects.table.cells.count, 7)
        XCTAssert(self.productListPageObjects.search.exists)
        
    }
}
