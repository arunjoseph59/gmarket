# README #

### Features
Gousto App will list and displays products from Gousto Market.  


### Requirements
iOS 13 or later
Compatible with iPhone


### Design pattern
The architectural design pattern MVVM-C (MVVM and Coordinator) is used for this application. 


### Used Third party Libraries
SDWebImage 

Justification:- This library provides an async image downloader with cache support. Trying to create an image downloader functionality with cache support is time consuming and little bit difficult task. Also this is a proven library with great support and updations. 

Alamofire

Justification:- Alamofire is a commonly used networking library for iOS. Also, I have created a  good network module  using this library. I tried to showcase that in this task. 


### Known issue
Three files ‘MockAPIManager’ , ‘MockPersistanceManager’ , ‘JsonLoader’ are sharing with Unit Test and App target. Need to be modularised.
