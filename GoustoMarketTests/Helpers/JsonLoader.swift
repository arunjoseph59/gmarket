//
//  JsonLoader.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

typealias JsonLoaderCompletion<T:Decodable> = (Result<T,Error>)-> Void

class JsonLoader {
    
    static let shared = JsonLoader()
    
    func loadJson<T:Decodable>(anyClass: AnyClass,filename fileName: String, responseType: T.Type, completion: @escaping (Result<T,Error>)-> Void) {
        if let url = Bundle(for: anyClass).url(forResource: fileName, withExtension: "json")  {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(responseType.self, from: data)
                completion(.success(jsonData))
            } catch let error{
                completion(.failure(error))
            }
        }
        else{
            completion(.failure(CustomError.somethingWentWrong))
        }
    }
    
    func loadJsonFromMain<T:Decodable>(anyClass: AnyClass,filename fileName: String, responseType: T.Type, completion: @escaping (Result<T,Error>)-> Void) {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json")  {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(responseType.self, from: data)
                completion(.success(jsonData))
            } catch let error{
                completion(.failure(error))
            }
        }
        else{
            completion(.failure(CustomError.somethingWentWrong))
        }
    }
}
