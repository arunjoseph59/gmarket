//
//  MockProductDetailViewModel.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 04/02/22.
//

import Foundation
@testable import GoustoMarket

class MockProductDetailViewModel: ProductDetailViewModelType {
    
    var product: Product?
    
    var getPrice_callCount = 0
    func getPrice() -> String {
        self.getPrice_callCount += 1
        
        return ""
    }
}
