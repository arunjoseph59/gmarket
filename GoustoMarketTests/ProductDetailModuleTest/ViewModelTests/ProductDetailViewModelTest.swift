//
//  ProductDetailViewModelTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 05/02/22.
//

import XCTest
@testable import GoustoMarket

class ProductDetailViewModelTest: XCTestCase {
    
    var sut: ProductDetailsViewModel!
    var mockProduct: Product!

    override func setUpWithError() throws {
        self.sut = ProductDetailsViewModel()
        self.getProduct()
        self.sut.product = mockProduct
    }

    override func tearDownWithError() throws {
        self.sut = nil
        self.mockProduct = nil
    }
    
    func test_getPrice_returns_currencySymbol_and_price_concatinated_when_price_empty() {
        self.sut.product?.list_price = ""
        
        
        let price = self.sut.getPrice()
        
        
        XCTAssertEqual(price, "")
    }
    
    func test_getPrice_returns_currencySymbol_and_price_concatinated_when_price_is_nil() {
        self.sut.product?.list_price = nil
        
        
        let price = self.sut.getPrice()
        
        
        XCTAssertEqual(price, "")
    }
    
    func test_getPrice_returns_currencySymbol_and_price_concatinated_when_product_is_nil() {
        self.sut.product = nil
        
        
        let price = self.sut.getPrice()
        
        
        XCTAssertEqual(price, "")
    }
    
    func test_getPrice_returns_currencySymbol_and_price_concatinated_when_price_is_available() {
        let price = self.sut.getPrice()
        
        
        XCTAssertEqual(price, "£6.95")
    }
}

extension ProductDetailViewModelTest {
    private func getProduct() {
        JsonLoader.shared.loadJson(anyClass: ProductDetailViewModelTest.self, filename: Constants.JsonFileName.product, responseType: Product.self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let product):
                self.mockProduct = product
            }
        }
    }
}
