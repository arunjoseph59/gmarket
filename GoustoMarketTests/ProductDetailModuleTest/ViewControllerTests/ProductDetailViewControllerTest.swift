//
//  ProductDetailViewControllerTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 04/02/22.
//

import XCTest
@testable import GoustoMarket

class ProductDetailViewControllerTest: XCTestCase {
    
    var sut: ProductDetailsViewController!
    var mockProduct: Product!
    var mockViewModel: ProductDetailViewModelType!
    
    override func setUpWithError() throws {
        self.sut = ProductDetailsViewController.instantiate()
        self.mockViewModel = MockProductDetailViewModel()
        self.sut.viewModel = self.mockViewModel
    }

    override func tearDownWithError() throws {
        self.sut = nil
        self.mockViewModel = nil
        self.mockProduct = nil
    }
    
    func test_datas_set_on_view_when_product_is_available() {
        self.getProduct()
        self.mockViewModel.product = self.mockProduct
        let viewModelMock = self.mockViewModel as? MockProductDetailViewModel
        
        
        self.sut.loadViewIfNeeded()
        
        
        XCTAssertEqual(self.sut.productTitle.text, "Borsao Macabeo")
        XCTAssertEqual(viewModelMock?.getPrice_callCount, 1)
    }
    
    func test_datas_set_on_view_when_product_is_not_available() {
        self.mockViewModel.product = nil
        let viewModelMock = self.mockViewModel as? MockProductDetailViewModel

        
        self.sut.loadViewIfNeeded()

        
        XCTAssertEqual(self.sut.productDescription.text, "")
        XCTAssertEqual(self.sut.productTitle.text, nil)
        XCTAssertEqual(self.sut.productPrice.text, "")
        XCTAssertEqual(self.sut.productImageView.image, UIImage(named: "productPlaceholder"))
        XCTAssertEqual(viewModelMock?.getPrice_callCount, 1)
    }
}

extension ProductDetailViewControllerTest {
    private func getProduct() {
        JsonLoader.shared.loadJson(anyClass: ProductDetailViewControllerTest.self, filename: Constants.JsonFileName.product, responseType: Product.self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let product):
                self.mockProduct = product
            }
        }
    }
}
