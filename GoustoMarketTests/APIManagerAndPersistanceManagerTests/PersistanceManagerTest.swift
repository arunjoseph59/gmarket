//
//  PersistanceManagerTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 04/02/22.
//

import XCTest
@testable import GoustoMarket

class PersistanceManagerTest: XCTestCase {
    
    var sut: MockPersistanceManager!
    private var mockProducts: [Product]!
    
    override func setUpWithError() throws {
        self.sut = MockPersistanceManager(endPoint: .products)
    }

    override func tearDownWithError() throws {
        self.sut = nil
        self.mockProducts = nil
    }
    
    func test_data_successfully_saved() {
        self.getProducts()
        let pathName = Constants.JsonFileName.products
    
        
        self.sut.saveToDisk(pathName: pathName, data: mockProducts) { (result) in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let success):
                XCTAssertTrue(success)
            }
        }
    }
    
    func test_data_saving_failed() {
        self.getProducts()
        let pathName = Constants.PathName.products
        self.sut.isFailed = true
        
        
        self.sut.saveToDisk(pathName: pathName, data: mockProducts) { (result) in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
                XCTFail()
            }
        }
    }
    
    func test_data_retrieving_success() {
        let pathName = Constants.PathName.products
        
        
        self.sut.retrieveFromDisk(pathName: pathName, typeOf: [Product].self) { (result) in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let products):
                XCTAssertTrue(products.count != 0)
                XCTAssertEqual(products.count, 7)
            }
        }
    }
    
    func test_data_retrieving_failed() {
        let pathName = Constants.PathName.products
        self.sut.isFailed = true

        
        self.sut.retrieveFromDisk(pathName: pathName, typeOf: [Product].self) { (result) in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success(let products): XCTAssertNotNil(products)
                XCTFail()
            }
        }
    }
}

extension PersistanceManagerTest {
    private func getProducts() {
        JsonLoader.shared.loadJsonFromMain(anyClass: PersistanceManagerTest.self, filename: Constants.JsonFileName.products, responseType: [Product].self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let products):
                self.mockProducts = products
            }
        }
    }
}
