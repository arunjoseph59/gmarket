//
//  MockAPIManager.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
@testable import GoustoMarket

class MockAPIManager: APIManagerTye {
    
    var isFailed = false
    var endPoint: EndPoint
    
    init(isFailed: Bool = false, endPoint: EndPoint) {
        self.isFailed = isFailed
        self.endPoint = endPoint
    }
    
    var request_callCount = 0
    func request<T: Decodable>(_ request: URLRequestBuilder, responseType: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        self.request_callCount += 1
        
        if self.isFailed{
            completion(.failure(CustomError.somethingWentWrong))
        }
        else  {
            self.getProducts(responseType: responseType) { (result) in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let responseOject):
                    completion(.success(responseOject))
                }
            }
        }
    }
}

extension MockAPIManager {
    
    private var jsonFileName: String {
        switch endPoint {
        case .products: return Constants.JsonFileName.products
        }
    }
    
    private func getProducts<T: Decodable>(responseType: T.Type, completion: @escaping (Result<T, Error>)-> Void) {
        JsonLoader.shared.loadJson(anyClass: MockAPIManager.self, filename: jsonFileName, responseType: responseType) { (result) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let responseObject):
                completion(.success(responseObject))
            }
        }
    }
}
