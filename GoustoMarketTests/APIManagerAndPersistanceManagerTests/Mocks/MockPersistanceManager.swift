//
//  MockPersistanceManager.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
@testable import GoustoMarket

class MockPersistanceManager: PersistaneManagerType {
    
    var isFailed = false
    var endPoint: EndPoint
    
    init(endPoint: EndPoint, isFailed: Bool = false) {
        self.isFailed = isFailed
        self.endPoint = endPoint
    }
    
    var saveToDisk_callCount = 0
    var saveToDisk_pathName: String = ""
    func saveToDisk<T: Codable>(pathName: String, data: T, completion: @escaping (Result<Bool, Error>) -> Void) {
        self.saveToDisk_callCount += 1
        self.saveToDisk_pathName = pathName
        
        if self.isFailed{
            completion(.failure( CustomError.somethingWentWrong))
        }
        else {
            completion(.success(true))
        }
    }
    
    var retrieveFromDisk_callCount = 0
    var retrieveFromDisk_pathName: String = ""
    func retrieveFromDisk<T: Codable>(pathName: String, typeOf: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        self.retrieveFromDisk_pathName = pathName
        self.retrieveFromDisk_callCount += 1
        
        if self.isFailed{
            completion(.failure(CustomError.somethingWentWrong))
        }
        else  {
            self.getProducts(responseType: typeOf) { (result) in
                switch result {
                case .failure(let error):
                    completion(.failure(error))
                case .success(let responseObject):
                    completion(.success(responseObject))
                }
            }
        }
    }
}

extension MockPersistanceManager {
    private var jsonFileNae: String {
        switch endPoint {
        case .products: return Constants.JsonFileName.products
        }
    }
    
    private func getProducts<T: Decodable>(responseType: T.Type, completion: @escaping (Result<T, Error>)-> Void) {
        JsonLoader.shared.loadJsonFromMain(anyClass: MockPersistanceManager.self, filename: jsonFileNae, responseType: responseType) { (result) in
            switch result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let responseObject):
                completion(.success(responseObject))
            }
        }
    }
}
