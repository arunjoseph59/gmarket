//
//  MockRouter.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
import Alamofire
@testable import GoustoMarket

enum MockRouter: URLRequestBuilder {
    
    case getMockProducst
    
    var endpoint: EndPoint {
        switch self {
        case .getMockProducst: return .products
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getMockProducst: return .get
        }
    }
}
