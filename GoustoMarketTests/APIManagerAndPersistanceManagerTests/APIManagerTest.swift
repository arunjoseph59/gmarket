//
//  APIManagerTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import XCTest
@testable import GoustoMarket

class APIManagerTest: XCTestCase {
    
    var sut: MockAPIManager!

    override func setUpWithError() throws {
        self.sut = MockAPIManager(endPoint: .products)
    }

    override func tearDownWithError() throws {
        self.sut = nil
    }
    
    func test_fetchData_success() {
        let request = MockRouter.getMockProducst
        
        
        self.sut.request(request, responseType: [Product].self) { (result) in
            switch result {
            case .failure(let error):
                XCTFail(error.localizedDescription)
            case .success(let products):
                XCTAssertNotNil(products)
                XCTAssertEqual(products.count, 7)
            }
        }
    }
    
    func test_fetchData_fails() {
        let request = MockRouter.getMockProducst
        self.sut.isFailed = true
        
        
        self.sut.request(request, responseType: [Product].self) { (result) in
            switch result {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .success:
               XCTFail("products is not nil")
            }
        }
    }
}
