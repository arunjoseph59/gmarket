//
//  ProductCoordinatorTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 05/02/22.
//

import XCTest
import UIKit
@testable import GoustoMarket

class ProductCoordinatorTest: XCTestCase {

    var productCoordinator: ProductCoordinator!
    var mockProduct: Product!
    
    override func setUpWithError() throws {
        self.productCoordinator = ProductCoordinator(navigationController: UINavigationController())
    }

    override func tearDownWithError() throws {
        self.productCoordinator = nil
        self.mockProduct = nil
    }
    
    func test_when_productListViewController_is_pushed() {
        self.productCoordinator.start(navigation: ProductNavigation.productVC)
        
        
        XCTAssertTrue(self.productCoordinator.navigationController.viewControllers.first is ProductListViewController)
    }
    
    func test_when_productDetailViewController_is_pushed() {
        self.getProducts()
        self.productCoordinator.start(navigation: ProductNavigation.productDetailVC(mockProduct))
        
        
        XCTAssertTrue(self.productCoordinator.navigationController.viewControllers.first is ProductDetailsViewController)
    }
}

extension ProductCoordinatorTest {
    private func getProducts() {
        JsonLoader.shared.loadJson(anyClass: ProductCoordinatorTest.self, filename: Constants.JsonFileName.product, responseType: Product.self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let product):
                self.mockProduct = product
            }
        }
    }
}
