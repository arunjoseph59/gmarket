//
//  MockProductCoordinator.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 05/02/22.
//

import Foundation
import UIKit
@testable import GoustoMarket

class MockProductCoordinator: CoordinatorType {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    var start_callCount = 0
    func start(navigation: Any) {
        self.start_callCount += 1
        
    }
}
