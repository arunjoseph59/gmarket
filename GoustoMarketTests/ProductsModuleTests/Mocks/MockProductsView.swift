//
//  MockProductsView.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
@testable import GoustoMarket

class MockProductsView: ProductListViewType {
    
    var coordinator: CoordinatorType?
    var viewModel: ProductViewModelType?
    var products: [Product] = [] {
        didSet{
            self.reloadView()
        }
    }
    
    var reload_callCount = 0
    func reloadView() {
        self.reload_callCount += 1
    }
    
    var showAlert_callCount = 0
    var showErrorAlert_arg: String = ""
    func showErrorAlert(with errorMessage: String) {
        self.showErrorAlert_arg = errorMessage
        self.showAlert_callCount += 1
    }
    
    var updateViewWithFetched_callCount = 0
    var updateViewWithFetched_args: (products: [Product]?, error: String?)
    func updateViewWithFetched(products: [Product]?, error: String?) {
        self.updateViewWithFetched_callCount += 1
        self.updateViewWithFetched_args.products = products
        self.updateViewWithFetched_args.error = error
        
        if let products = products { self.products = products }
        if let error = error { self.showErrorAlert(with: error) }
    }
}
