//
//  MockProductsViewModel.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
@testable import GoustoMarket

class MockProductsViewModel: ProductViewModelType {
    
    var products: [Product]? 
    var view: ProductListViewType?
    private var mockProducts: [Product] = []
    
    func attach(view: ProductListViewType) {
        self.view = view
    }
    
    var fetchProducts_callCount = 0
    func fetchProducts() {
        self.fetchProducts_callCount += 1
    }
    
    func filterProductsBasedOn(searchedString: String) {
        
    }
}
