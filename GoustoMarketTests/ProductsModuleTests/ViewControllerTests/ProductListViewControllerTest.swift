//
//  ProductListViewControllerTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import XCTest
@testable import GoustoMarket

class ProductListViewControllerTest: XCTestCase {
    
    var sut: ProductListViewController!
    var tableView: UITableView!
    var mockProducts: [Product]!
    var mockcoordinator: CoordinatorType!

    override func setUpWithError() throws {
        self.sut = ProductListViewController.instantiate()
        self.mockcoordinator = MockProductCoordinator()
        self.getProducts()
        _ = self.sut.view
        self.tableView = self.sut.productsTableView
        self.tableView.dataSource = self.sut
        self.tableView.delegate = self.sut
        self.tableView.register(cell: ProductTableViewCell.self)
    }

    override func tearDownWithError() throws {
        self.sut = nil
        self.tableView = nil
        self.mockProducts = nil
        self.mockcoordinator = nil
    }
    
    func test_cell_for_row_when_data_is_loaded() {
        self.sut.products = self.mockProducts
        
        
        let cell = self.sut.tableView(self.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? ProductTableViewCell
        let price = self.sut.viewModel?.productPrice(price: "6.95")
        
        
        XCTAssertNotNil(cell?.productTitleLabel)
        XCTAssertNotNil(cell?.productImageView)
        XCTAssertNotNil(cell?.productPriceLabel)
        XCTAssertEqual(cell?.productTitleLabel.text, "Borsao Macabeo")
        XCTAssertEqual(cell?.productPriceLabel.text, price)
    }
    
    func test_number_of_rows_when_data_is_loaded() {
        self.sut.products = self.mockProducts
        
        
        let numberOfRows = self.sut.tableView(self.tableView, numberOfRowsInSection: 0)
        
        
        XCTAssertEqual(numberOfRows, 7)
    }
    
    func test_didSelect_when_tap_on_cell() {
        self.sut.products = self.mockProducts
        self.sut.coordinator = self.mockcoordinator
        let coordinatorMock = self.mockcoordinator as? MockProductCoordinator
        
        
        sut.tableView(self.tableView, didSelectRowAt: IndexPath(row: 0, section: 0))

        
        XCTAssertEqual(coordinatorMock?.start_callCount, 1)
    }
    
    func test_update_datas_when_product_fetched_succesfully() {
        self.sut.updateViewWithFetched(products: self.mockProducts, error: nil)
        
        
        XCTAssertEqual(self.sut.products.count, 7)
    }
    
    func test_update_datas_when_product_fetched_fails_with_error() {
        self.sut.updateViewWithFetched(products: nil, error: "Some error")
        
        
        XCTAssertEqual(self.sut.products.count, 0)
    }

}

extension ProductListViewControllerTest {
    private func getProducts() {
        JsonLoader.shared.loadJson(anyClass: ProductListViewControllerTest.self, filename: Constants.JsonFileName.products, responseType: [Product].self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let products):
                self.mockProducts = products
            }
        }
    }
}
