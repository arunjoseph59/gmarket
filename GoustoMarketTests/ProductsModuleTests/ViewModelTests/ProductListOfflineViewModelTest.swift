//
//  ProductListOfflineViewModelTest.swift
//  GoustoMarketTests
//
//  Created by A-10474 on 03/02/22.
//

import XCTest
@testable import GoustoMarket

class ProductListOfflineViewModelTest: XCTestCase {
    
    var mockProducts: [Product]!
    var sut: ProductListOfflineViewModel!
    var mockpersistanceManager: PersistaneManagerType!
    var mockView: ProductListViewType!

    override func setUpWithError() throws {
        self.mockpersistanceManager = MockPersistanceManager(endPoint: .products)
        self.mockView = MockProductsView()
        self.sut = ProductListOfflineViewModel(persistantManager: self.mockpersistanceManager)
    }

    override func tearDownWithError() throws {
        self.sut = nil
        self.mockpersistanceManager = nil
        self.mockView = nil
        self.mockProducts = nil
    }
    
    func test_when_initialise() {
        self.sut = ProductListOfflineViewModel(persistantManager: self.mockpersistanceManager)
        
        
        XCTAssertNotNil(self.sut.persistanceManager)
    }

    func test_price_string_when_price_is_not_empty() {
        let price = self.sut.productPrice(price: "30")
        
        
        XCTAssertNotEqual(price, "")
        XCTAssertEqual(price, "£30")
    }
    
    func test_price_string_when_price_is_empty() {
        let price = self.sut.productPrice(price: "")
        
        
        XCTAssertEqual(price, "")
    }
    
    func test_price_when_price_is_nil() {
        let price = self.sut.productPrice(price: nil)
        
        
        XCTAssertEqual(price, "")
    }
    
    func test_view_is_available() {
        self.sut.attach(view: self.mockView)
        
        
        XCTAssertNotNil(self.sut.view)
    }
    
    func test_retrieveFrom_local_success() {
        let persitanceMockManager = self.mockpersistanceManager as? MockPersistanceManager
        let viewMock = self.mockView as? MockProductsView
        
        
        self.sut.attach(view: self.mockView)
        self.sut.fetchProducts()
        
        
        XCTAssertEqual(viewMock?.updateViewWithFetched_callCount, 1)
        XCTAssertNil(viewMock?.updateViewWithFetched_args.error)
        XCTAssertEqual(viewMock?.updateViewWithFetched_args.products?.count, 7)
        XCTAssertEqual(self.mockView.products.count, 7)
        XCTAssertEqual(persitanceMockManager?.saveToDisk_callCount, 0)
        XCTAssertEqual(persitanceMockManager?.retrieveFromDisk_callCount, 1)
        XCTAssertEqual(persitanceMockManager?.retrieveFromDisk_pathName, Constants.PathName.products)
        XCTAssertEqual(viewMock?.reload_callCount, 1)
        XCTAssertEqual(viewMock?.showAlert_callCount, 0)
    }
    
    func test_retrieve_from_loacal_fails() {
        let persitanceMockManager = self.mockpersistanceManager as? MockPersistanceManager
        let viewMock = self.mockView as? MockProductsView
        persitanceMockManager?.isFailed = true
        
        
        self.sut.attach(view: self.mockView)
        self.sut.fetchProducts()
        
        
        XCTAssertEqual(self.mockView.products.count, 0)
        XCTAssertNotNil(viewMock?.showErrorAlert_arg)
        XCTAssertEqual(viewMock?.updateViewWithFetched_callCount, 1)
        XCTAssertEqual(persitanceMockManager?.saveToDisk_callCount, 0)
        XCTAssertEqual(persitanceMockManager?.retrieveFromDisk_callCount, 1)
        XCTAssertEqual(persitanceMockManager?.retrieveFromDisk_pathName, Constants.PathName.products)
        XCTAssertEqual(viewMock?.reload_callCount, 0)
        XCTAssertEqual(viewMock?.showAlert_callCount, 1)
    }
    
    func test_gives_correct_products_when_search_witht_productTitle() {
        self.getProducts()
        self.sut.view = self.mockView
        self.sut.view?.products = self.mockProducts!
        self.sut.products = self.mockProducts
        
        
        self.sut.filterProductsBasedOn(searchedString: "Borsao Macabeo")
        
        
        XCTAssertEqual(self.sut.view?.products.count, 1)
        XCTAssertEqual(self.sut.view?.products[0].title, "Borsao Macabeo")
    }
    
    func test_gives_no_products_when_search_with_wrong_productTitle() {
        self.getProducts()
        self.sut.view = self.mockView
        self.sut.view?.products = self.mockProducts!
        self.sut.products = self.mockProducts
        
        
        self.sut.filterProductsBasedOn(searchedString: "ABCDEF")
        
        
        XCTAssertEqual(self.sut.view?.products.count, 0)
    }
    
    func test_gives_all_products_when_search_with_empty_string() {
        self.getProducts()
        self.sut.view = self.mockView
        self.sut.view?.products = self.mockProducts!
        self.sut.products = self.mockProducts
        
        
        self.sut.filterProductsBasedOn(searchedString: "")
        
        
        XCTAssertEqual(self.sut.view?.products.count, 7)
    }
}

extension ProductListOfflineViewModelTest {
    private func getProducts() {
        JsonLoader.shared.loadJson(anyClass: ProductListOfflineViewModelTest.self, filename: Constants.JsonFileName.products, responseType: [Product].self) { (result) in
            switch result {
            case .failure:
                print("Json parsing failed.. please check")
                break
            case .success(let products):
                self.mockProducts = products
            }
        }
    }
}
