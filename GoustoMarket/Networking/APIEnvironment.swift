//
//  APIEnvironment.swift
//  GoustoMarket
//
//  Created by A-10474 on 01/02/22.
//

import Foundation

public enum APIEnvironment {
    
    static let current: APIEnvironment = .development
    
    case development
    case production
    
    var baseURL: String {
        switch self {
        case .development: return Constants.APIConstants.development
        case .production: return Constants.APIConstants.production
        }
    }
}
