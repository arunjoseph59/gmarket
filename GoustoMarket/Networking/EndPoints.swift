//
//  EndPoints.swift
//  GoustoMarket
//
//  Created by A-10474 on 01/02/22.
//

import Foundation

enum EndPoint: String {
    
    case products = "/products"
}
