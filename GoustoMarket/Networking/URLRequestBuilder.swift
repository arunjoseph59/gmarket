//
//  URLRequestBuilder.swift
//  GoustoMarket
//
//  Created by A-10474 on 01/02/22.
//

import Foundation
import Alamofire

protocol URLRequestBuilder: URLRequestConvertible {
    
    var baseURL: URL { get }
    var endpoint: EndPoint { get }
    var headers: HTTPHeaders { get }
    var requestURL: URL { get }
    var method: HTTPMethod { get }
    var parameterEncoder: ParameterEncoder { get }
    func asURLRequest() throws -> URLRequest
}

extension URLRequestBuilder {
    
    var baseURL: URL {
        return URL(string: APIEnvironment.current.baseURL)!
    }
    
    var requestURL: URL {
       return baseURL.appendingPathComponent(endpoint.rawValue)
    }
    
    var headers: HTTPHeaders {
        var values = HTTPHeaders()
        values["Content-Type"] = "application/json"
        return values
    }
    
    var baseRequest: URLRequest {
        var request = URLRequest(url: requestURL)
        request.httpMethod = method.rawValue
        headers.forEach { request.setValue($0.value, forHTTPHeaderField: $0.name) }
        return request
    }
    
    var parameterEncoder: ParameterEncoder {
        return URLEncodedFormParameterEncoder()
    }
    
    func asURLRequest() throws -> URLRequest {
        return baseRequest
    }
}
