//
//  APIManagerType.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation

protocol APIManagerTye {
    func request<T: Decodable>(_ request: URLRequestBuilder, responseType: T.Type, completion: @escaping (Result<T, Error>) -> Void)
}
