//
//  APIManager.swift
//  GoustoMarket
//
//  Created by A-10474 on 01/02/22.
//

import Foundation
import Alamofire

final class APIManager: APIManagerTye {
    
    private let session = Session()
    
    func request<T: Decodable>(_ request: URLRequestBuilder, responseType: T.Type, completion: @escaping (Result<T, Error>) -> Void)  {
        self.session.request(request).validate().responseDecodable(of: APIResponse<T>.self) { (response) in
            switch response.result {
            case .failure(let error):
                completion(.failure(error))
            case .success(let data):
                completion(.success(data.data))
            }
        }        
    }
    
}

//MARK: First layer API response
struct APIResponse<T: Decodable>: Decodable {
    var status: String
    var data: T
}
