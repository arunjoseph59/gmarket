//
//  PersistanceManager.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

class PersistanceManager: PersistaneManagerType {
    
    //MARK: Save to local storage
    func saveToDisk<T: Codable>(pathName: String, data: T, completion: @escaping (Result<Bool, Error>) -> Void) {
        guard let url = self.getDocumentsURL()?.appendingPathComponent(pathName) else {
            completion(.failure(CustomError.somethingWentWrong))
            return
        }
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(data)
            try data.write(to: url, options: [])
            completion(.success(true))
        }
        catch (let error) {
            completion(.failure(error))
        }
    }
    
    //MARK: Retrieve from local storage
    func retrieveFromDisk<T: Codable>(pathName: String, typeOf: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
        guard let url = self.getDocumentsURL()?.appendingPathComponent(pathName) else {
            completion(.failure(CustomError.somethingWentWrong))
            return
        }
        let decoder = JSONDecoder()
        do{
            let data = try Data(contentsOf: url, options: [])
            let decodedData = try decoder.decode(typeOf.self, from: data)
            completion(.success(decodedData))
        }
        catch (let error) {
            completion(.failure(error))
        }
    }
    
    //MARK: Get documents directory url
    private func getDocumentsURL()-> URL? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return url
    }
}
