//
//  PersistaneManagerType.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

protocol PersistaneManagerType {
    
    func saveToDisk<T: Codable>(pathName: String, data: T, completion: @escaping (Result<Bool, Error>)-> Void)
    func retrieveFromDisk<T: Codable>(pathName: String, typeOf: T.Type, completion: @escaping (Result<T, Error>)-> Void)
}
