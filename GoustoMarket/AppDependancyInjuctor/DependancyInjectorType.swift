//
//  DependancyInjectorType.swift
//  GoustoMarket
//
//  Created by A-10474 on 05/02/22.
//

import Foundation
import UIKit

protocol DependancyInjectorType {
    
    //MARK: View models
    var productViewmodel: ProductViewModelType { get }
    var productDetailViewModel: ProductDetailViewModelType { get }
    
    //MARK: Coordinators
    var appFlowCoordinator: CoordinatorType { get }
    func getProductCoordinator(navigationcontroller: UINavigationController)-> CoordinatorType
}
