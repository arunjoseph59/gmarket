//
//  DependancyInjuctor.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import UIKit
import Alamofire

class DependencyInjector: DependancyInjectorType {
    
    private var window: UIWindow
    
    //MARK: APP Navigation Controlers
    private var productNavigationController: UINavigationController!
    
    init(window: UIWindow) {
        self.window = window
    }
    
    private var apiManager: APIManagerTye {
        return APIManager()
    }
    
    private var persistanceManager: PersistaneManagerType {
        return PersistanceManager()
    }
}

//MARK: ViewModels
extension DependencyInjector {
    var productViewmodel: ProductViewModelType {
        
        switch AppConfiguration.shared.appEnvironment {
        case .normal:
            return AppConfiguration.shared.isOnline ? ProductListOnlineViewModel(apiManager: self.apiManager, persistanceManager: persistanceManager): ProductListOfflineViewModel(persistantManager: persistanceManager)
        case .uiTesting(let isErrorOccured):
            let mockAPIManager = MockAPIManager(isFailed: isErrorOccured, endPoint: .products)
            let mockPersistanceManager = MockPersistanceManager(endPoint: .products, isFailed: isErrorOccured)
            return AppConfiguration.shared.isOnline ? ProductListOnlineViewModel(apiManager: mockAPIManager, persistanceManager: persistanceManager): ProductListOfflineViewModel(persistantManager: mockPersistanceManager)
       
        }
    }
    
    var productDetailViewModel: ProductDetailViewModelType {
        return ProductDetailsViewModel()
    }
}

//MARK: Coordinators
extension DependencyInjector {
    var appFlowCoordinator: CoordinatorType {
        let navigationController = UINavigationController()
        self.productNavigationController = navigationController
        return AppFlowCoordinator(window: self.window, navigationController: productNavigationController)
    }
    
    func getProductCoordinator(navigationcontroller: UINavigationController) -> CoordinatorType {
        return ProductCoordinator(navigationController: navigationcontroller)
    }
}
