//
//  ErrorView.swift
//  GoustoMarket
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import UIKit

protocol ErrorViewType {
    
    var errorMessage: String? { get set }
    var errorImage: UIImage? { get set }
    var hideClose: Bool? { get set }
    
}

class ErrorView: UIView, ErrorViewType {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    
    var errorMessage: String? {
        didSet{
            self.errorMessageLabel.text = errorMessage
        }
    }
    var errorImage: UIImage? {
        didSet {
            self.errorImageView.image = errorImage
        }
    }
    var hideClose: Bool? {
        didSet {
            self.closeButton.isHidden = hideClose ?? false
        }
    }
    
    //MARK: Inits
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initSubviews()
        self.addAccesibiltyIdentifiers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initSubviews()
        self.addAccesibiltyIdentifiers()
    }
    
    //MARK: add subview
    private func initSubviews() {
        let nib = UINib(nibName: "ErrorView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        self.contentView.frame = bounds
        self.addSubview(contentView)
    }
    
    @IBAction func closeButtonAction(_ sender: Any) {
        self.removeFromSuperview()
    }
}

extension ErrorView {
    private func addAccesibiltyIdentifiers() {
        self.closeButton.accessibilityIdentifier = Constants.AccesibilityIdentifier.erroViewCloseButton
        self.errorImageView.accessibilityIdentifier = Constants.AccesibilityIdentifier.erroViewErrorImageView
        self.errorMessageLabel.accessibilityIdentifier = Constants.AccesibilityIdentifier.erroViEwerrorMessageLabel
        self.contentView.accessibilityIdentifier = Constants.AccesibilityIdentifier.errorView
    }
}

