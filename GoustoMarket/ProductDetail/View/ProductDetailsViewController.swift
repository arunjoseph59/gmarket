//
//  ProductDetailsViewController.swift
//  GoustoMarket
//
//  Created by A-10474 on 04/02/22.
//

import UIKit

class ProductDetailsViewController: UIViewController, Storyboarded {
    static var storyboardName: String { StoryboardName.main.rawValue }

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var productDescription: UITextView!
    
    var viewModel: ProductDetailViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDatas()
        self.setAccesibiltyIdentifiers()
    }
    
    private func setDatas() {
        self.productTitle.text = self.viewModel?.product?.title
        self.productImageView.loadImage(with: self.viewModel?.product?.images?.values?.url ?? "")
        self.productPrice.text = self.viewModel?.getPrice()
        self.productDescription.text = self.viewModel?.product?.description
    }
}

extension ProductDetailsViewController {
    private func setAccesibiltyIdentifiers() {
        self.productImageView.accessibilityIdentifier = Constants.AccesibilityIdentifier.productDetailImageView
        self.productTitle.accessibilityIdentifier = Constants.AccesibilityIdentifier.productDetailTitle
        self.productPrice.accessibilityIdentifier = Constants.AccesibilityIdentifier.productDetailPrice
        self.productDescription.accessibilityIdentifier = Constants.AccesibilityIdentifier.productDetailDescription
    }
}
