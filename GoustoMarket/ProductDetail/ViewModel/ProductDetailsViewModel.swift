//
//  ProductDetailsViewModel.swift
//  GoustoMarket
//
//  Created by A-10474 on 04/02/22.
//

import Foundation

class ProductDetailsViewModel: ProductDetailViewModelType {
    
    var product: Product?
    
    func getPrice() -> String {
        guard let price = self.product?.list_price else { return "" }
        if !price.isEmpty {
            return "£"+price
        }
        return ""
    }
}
