//
//  ProductDetailViewModelType.swift
//  GoustoMarket
//
//  Created by A-10474 on 04/02/22.
//

import Foundation

protocol ProductDetailViewModelType {
    
    var product: Product? { get set }
    func getPrice()-> String
}
