//
//  ProductListViewModel.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import Alamofire

class ProductListOnlineViewModel: ProductViewModelType, APICallingType, OfflineStorageType {
    
    var products: [Product]?
    var apiManager: APIManagerTye
    var persistanceManager: PersistaneManagerType
    var view: ProductListViewType?
    
    init(apiManager: APIManagerTye, persistanceManager: PersistaneManagerType) {
        self.apiManager = apiManager
        self.persistanceManager = persistanceManager
    }
    
    func attach(view: ProductListViewType) {
        self.view = view
    }
    
    func fetchProducts() {
        let productsRequest = ProductsRequest()
        let request = ProductRouter.getProducts(params: productsRequest)
        self.apiManager.request(request, responseType: [Product].self) {[weak self] (result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                self?.view?.updateViewWithFetched(products: nil, error: CustomError.pleaseTryAgain.errorDescription)
            case .success(let products):
                self?.saveToDisk(products: products)
                self?.products = products
                self?.view?.updateViewWithFetched(products: products, error: nil)
            }
        }
    }
    
    func filterProductsBasedOn(searchedString: String) {
        guard let products = self.products else { return }
        let searchedProducts = products.filter{$0.title.contains(searchedString)}
        self.view?.products = searchedString.isEmpty ? products: searchedProducts
    }
    
    //MARK: Save to File manager
    private func saveToDisk(products: [Product]) {
        self.persistanceManager.saveToDisk(pathName: Constants.PathName.products, data: products) { (result) in
            switch result {
            case .failure(let error):
                print("failed with error \(error.localizedDescription)")
            case .success(let success):
                print("is saved successfully \(success)")
            }
        }
    }
}
