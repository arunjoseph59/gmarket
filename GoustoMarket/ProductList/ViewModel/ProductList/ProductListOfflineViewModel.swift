//
//  ProductListOfflineViewModel.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

class ProductListOfflineViewModel: ProductViewModelType, OfflineStorageType  {
    
    var products: [Product]?
    var persistanceManager: PersistaneManagerType
    var view: ProductListViewType?
    
    init(persistantManager: PersistaneManagerType) {
        self.persistanceManager = persistantManager
    }
    
    func attach(view: ProductListViewType) {
        self.view = view
    }
    
    func fetchProducts() {
        persistanceManager.retrieveFromDisk(pathName: Constants.PathName.products, typeOf: [Product].self) { [weak self] (result) in
            switch result {
            case .failure(let error):
                print(error.localizedDescription)
                self?.view?.updateViewWithFetched(products: nil, error: CustomError.pleaseTryAgain.errorDescription)
            case .success(let products):
                self?.products = products
                self?.view?.updateViewWithFetched(products: products, error: nil)
            }
        }
    }
    
    func filterProductsBasedOn(searchedString: String) {
        guard let products = self.products else { return }
        let searchedProducts = products.filter{$0.title.contains(searchedString)}
        self.view?.products = searchedString.isEmpty ? products: searchedProducts
    }
}

