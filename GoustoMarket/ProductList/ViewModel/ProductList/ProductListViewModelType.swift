//
//  ProductListViewModelType.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import UIKit

protocol ProductViewModelType {
    
    var products: [Product]? { get }
    var view: ProductListViewType? { get }
    func attach(view: ProductListViewType)
    func fetchProducts()
    func productPrice(price: String?)-> String
    func filterProductsBasedOn(searchedString: String)
}

extension ProductViewModelType {
    
    func productPrice(price: String?) -> String {
        guard let price = price else { return "" }
        if !price.isEmpty {
            return "£"+price
        }
        return ""
    }
}
