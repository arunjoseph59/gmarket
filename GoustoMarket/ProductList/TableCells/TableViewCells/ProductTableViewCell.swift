//
//  ProductTableViewCell.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.applyStyle()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.productImageView.image = nil
        self.productTitleLabel.text = nil
        self.productPriceLabel.text = nil
    }
    
    private func applyStyle() {
        self.selectionStyle = .none
    }
    
    
    private func setAccesibilityIdentifier() {
        self.productImageView.image?.accessibilityIdentifier = Constants.AccesibilityIdentifier.productImageProductTableCell
        self.productTitleLabel.accessibilityIdentifier = Constants.AccesibilityIdentifier.ProductTitleLableTableCell
        self.productPriceLabel.accessibilityIdentifier = Constants.AccesibilityIdentifier.productPriceLabelTableCell
    }
}
