//
//  ProductListViewType.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation

protocol ProductListViewType {
    
    var coordinator: CoordinatorType? { get }
    var viewModel: ProductViewModelType? { get }
    var products:[Product] { get set }
    func reloadView()
    func showErrorAlert(with errorMessage: String)
    func updateViewWithFetched(products: [Product]?, error: String?)
}
