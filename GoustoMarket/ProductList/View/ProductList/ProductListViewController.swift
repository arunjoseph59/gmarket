//
//  ProductListViewController.swift
//  GoustoMarket
//
//  Created by A-10474 on 01/02/22.
//

import UIKit

class ProductListViewController: UIViewController, ProductListViewType, Storyboarded {
    
    static var storyboardName: String { StoryboardName.main.rawValue }
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var productsTableView: UITableView!
    
    var coordinator: CoordinatorType?
    var viewModel: ProductViewModelType?
    var products: [Product] = [] {
        didSet {
            self.reloadView()
        }
    }
    lazy var erroView: ErrorViewType? = {
        guard let coordinator = self.coordinator as? ProductCoordinator else { return nil }
        let errorView = coordinator.getErrorView(with: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height))
        return errorView
    }()
    
    //MARK: Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.productsTableView.register(cell: ProductTableViewCell.self)
        self.viewModel?.attach(view: self)
        self.setAccessibilityIdentifiers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.activityStartAnimating()
        self.searchBar.text = ""
        self.viewModel?.fetchProducts()
    }
    
    //MARK: ProductListViewType method
    func reloadView() {
        DispatchQueue.main.async {
            if let errorView = self.erroView as? ErrorView {
                errorView.removeFromSuperview()
            }
            self.productsTableView.reloadData()
        }
    }
    
    func updateViewWithFetched(products: [Product]?, error: String?) {
        self.view.activityStopAnimating()
        if let products = products { self.products = products }
        if let error = error { self.showErrorAlert(with: error) }
    }
    
    func showErrorAlert(with errorMessage: String) {
        guard let errorView = self.erroView as? UIView else { return }
        self.view.addSubview(errorView)
        self.erroView?.errorMessage = errorMessage
        self.erroView?.hideClose = false
    }
    
    //MARK: Set accessibility identifiers
    private func setAccessibilityIdentifiers() {
        self.productsTableView.accessibilityIdentifier = Constants.AccesibilityIdentifier.productsTableView
    }
}

//MARK: UITableViewDataSource, UITableViewDelegate
extension ProductListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(cell: ProductTableViewCell.self, forIndexPath: indexPath)
        cell.productPriceLabel.text = viewModel?.productPrice(price: products[indexPath.row].list_price)
        cell.productTitleLabel.text = products[indexPath.row].title
        cell.productImageView.loadImage(with: products[indexPath.row].images?.values?.url ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        self.coordinator?.start(navigation: ProductNavigation.productDetailVC(self.products[indexPath.row]))
    }
}

//MARK: UISearchBarDelegate
extension ProductListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.viewModel?.filterProductsBasedOn(searchedString: searchText)
    }
}
