//
//  Product.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation

struct Product: Codable {
    let id: String?
    let sku: String?
    let title: String
    let description: String?
    var list_price: String?
    let is_vatable: Bool
    let is_for_sale: Bool
    let age_restricted: Bool
    let box_limit: Int?
    let always_on_menu: Bool
    let volume: Int?
    let zone: String?
    let created_at: String?
    let attributes: [Attributes]?
    let images: Images?
}

struct Attributes: Codable {
    let id: String?
    let title: String?
    let unit: String?
    let value: String?
}

struct Images: Codable {
    let values: Values?
    
    enum CodingKeys: String, CodingKey {
        case values = "750"
    }
}

struct Values: Codable {
    let src: String?
    let url: String?
    let width: Int?
}

