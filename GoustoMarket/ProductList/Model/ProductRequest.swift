//
//  ProductRequest.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

struct ProductsRequest: Encodable {
    let includes = ["categories","attributes"]
    let image_sizes = ["750"]
}
