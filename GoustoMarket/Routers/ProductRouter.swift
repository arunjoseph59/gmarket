//
//  ProductRouter.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import Alamofire

enum ProductRouter: URLRequestBuilder {
        
    case getProducts(params: ProductsRequest)
    
    var endpoint: EndPoint{
        switch self {
        case .getProducts: return .products
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getProducts: return .get
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var request = baseRequest
        switch self {
        case .getProducts(let params):
            request = try parameterEncoder.encode(params, into: request)
        }
        return request
    }
}
