//
//  UIImageView+Extensions.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    
    func loadImage(with URLString: String) {
        switch AppConfiguration.shared.appEnvironment {
        case .normal:
            let placeholder = UIImage(named: Constants.Placeholders.productPlaceholder) ///image named productPlaceholder must be in assets
            guard let imageUrl = URL(string: URLString) else{self.image = placeholder; return}
            self.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.sd_imageIndicator?.startAnimatingIndicator()
            sd_setImage(with: imageUrl) { (image, error, cacheType, url) in
                self.sd_imageIndicator?.stopAnimatingIndicator()
                self.image = image != nil ? image: placeholder
            }
        case .uiTesting:
            self.image = UIImage(named: Constants.Placeholders.productPlaceholder) ///image named productPlaceholder must be in assets
        }
        
    }
}
