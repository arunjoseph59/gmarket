//
//  UITableView+Extensions.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
import UIKit

extension UITableView {
    
    func register(cell nib: UITableViewCell.Type) {
        let cellNib = UINib(nibName: String(describing: nib), bundle: nil)
        register(cellNib, forCellReuseIdentifier: String(describing: nib))
    }
    
    func dequeue<T: UITableViewCell>(cell type: T.Type, forIndexPath indexPath: IndexPath) -> T {
        let identifier = String(describing: type.self)
        guard let cell = dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? T else {
            fatalError("Check if cell identifier and class name are same")
        }
        return cell
    }
}
