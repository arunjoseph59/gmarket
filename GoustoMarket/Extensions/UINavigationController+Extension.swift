//
//  UINavigationController+Extension.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation
import UIKit

extension UINavigationController {
    func setTransparentbackground() {
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.isTranslucent = true
        self.view.backgroundColor = .clear
    }

    func setAppThemeStyle() {
        navigationBar.barTintColor = .white
        navigationBar.tintColor = .black
        navigationBar.titleTextAttributes = [.foregroundColor: UIColor.black, .font: UIFont.systemFont(ofSize: 20.0, weight: .heavy)]
    }
}
