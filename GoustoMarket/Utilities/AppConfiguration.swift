//
//  AppConfiguration.swift
//  GoustoMarket
//
//  Created by A-10474 on 05/02/22.
//

import Foundation
import UIKit
import Alamofire

enum AppEnvironment {
    case uiTesting(isErrorOccured: Bool = false)
    case normal
}

class AppConfiguration {
    
    static let shared = AppConfiguration()
    
    var window: UIWindow!
    var appEnvironment: AppEnvironment = .normal
    var isOnline: Bool {
        NetworkReachabilityManager()?.isReachable ?? false
    }
    
    var dependencyInjector: DependancyInjectorType {
        return DependencyInjector(window: self.window)
    }
}
