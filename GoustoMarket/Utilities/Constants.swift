//
//  Constants.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

struct Constants {
    
    //MARK: Placeholders
    struct Placeholders {
        static let productPlaceholder = "productPlaceholder"
    }
    
    //MARK: PathNames
    struct PathName {
        static let products = "products"
    }
    
    //MARK: JsonFileNames
    struct JsonFileName {
        static let products = "MockProducts"
        static let product = "MockProduct"
    }
    
    //MARK: ViewControllerTitles
    struct ViewControllerTitle {
        static let gousto = "Gousto"
        static let details = "Details"
    }
    
    //MARK: Alert Titles
    struct AlertTitle {
        static let alert = "Alert"
    }
    
    //MARK: AlertButton Title
    struct AlertButtonTitle {
        static let ok = "OK"
    }
    
    struct AccesibilityIdentifier {
        
        //MARK: ProductListViewController
        static let productsTableView = "productsTableView"
        
        
        //MARK: ProductLsTableViewCell
        static let productImageProductTableCell = "productImageProductTableCell"
        static let ProductTitleLableTableCell = "ProductTitleLableTableCell"
        static let productPriceLabelTableCell = "productPriceLabelTableCell"
        
        //MARK: ProductDetailsViewController
        static let productDetailImageView = "productDetailImageView"
        static let productDetailTitle = "productDetailTitle"
        static let productDetailPrice = "productDetailPrice"
        static let productDetailDescription = "productDetailDescription"
        
        //MARK: ErrorView
        static let erroViewCloseButton = "ErroViewCloseButton"
        static let erroViewErrorImageView = "ErroViewErrorImageView"
        static let erroViEwerrorMessageLabel = "ErroViEwerrorMessageLabel"
        static let errorView = "ErrorView"
    }
    
    //MARK: API Constants
    struct APIConstants {
        static let development = "https://api.gousto.co.uk/products/v2.0"
        static let production = "" /// ///return production base url
    }
}


