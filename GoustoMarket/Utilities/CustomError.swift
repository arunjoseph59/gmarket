//
//  CustomError.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

enum CustomError {
    case somethingWentWrong
    case pleaseTryAgain
}

extension CustomError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .somethingWentWrong: return "Something went wrong."
        case .pleaseTryAgain: return "Could not fetch the data! Please try again after sometimes."
        }
    }
}
