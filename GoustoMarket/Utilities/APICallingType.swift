//
//  APICallingType.swift
//  GoustoMarket
//
//  Created by A-10474 on 03/02/22.
//

import Foundation

protocol APICallingType {
    var apiManager: APIManagerTye { get }
}
