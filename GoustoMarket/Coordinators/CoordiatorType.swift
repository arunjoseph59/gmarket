//
//  CoordiatorType.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import UIKit

protocol CoordinatorType {
    var navigationController: UINavigationController { get set }
    func start(navigation: Any)
}
