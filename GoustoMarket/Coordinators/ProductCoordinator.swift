//
//  ProductCoordinator.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import UIKit

enum ProductNavigation {
    case productVC
    case productDetailVC(Product?)
}

class ProductCoordinator: CoordinatorType, ErroViewDisplayable {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start(navigation: Any) {
        guard let productNavigation = navigation as? ProductNavigation else {
            print(navigation)
            return }
        
        switch productNavigation {
        
        case .productDetailVC(let product):
            let productDetailViewController = ProductDetailsViewController.instantiate()
            productDetailViewController.viewModel = AppConfiguration.shared.dependencyInjector.productDetailViewModel
            productDetailViewController.viewModel?.product = product
            productDetailViewController.title = Constants.ViewControllerTitle.details
            self.navigationController.pushViewController(productDetailViewController, animated: true)
            
        case .productVC:
            let productViewController = ProductListViewController.instantiate()
            productViewController.viewModel = AppConfiguration.shared.dependencyInjector.productViewmodel
            productViewController.coordinator = self
            productViewController.title = Constants.ViewControllerTitle.gousto
            self.navigationController.pushViewController(productViewController, animated: true)
        }
    }
    
    //MARK: ErroViewDisplayable
    func getErrorView(with frame: CGRect) -> ErrorViewType {
        return ErrorView(frame: frame)
    }
}
