//
//  ErroViewDisplayable.swift
//  GoustoMarket
//
//  Created by A-10474 on 06/02/22.
//

import Foundation
import UIKit

protocol ErroViewDisplayable {
    func getErrorView(with frame: CGRect)-> ErrorViewType 
}
