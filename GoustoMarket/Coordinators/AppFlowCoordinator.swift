//
//  AppFlowCoordinator.swift
//  GoustoMarket
//
//  Created by A-10474 on 02/02/22.
//

import Foundation
import UIKit

class AppFlowCoordinator: NSObject, CoordinatorType {
    
    var navigationController: UINavigationController
    private var window: UIWindow

    init(window: UIWindow, navigationController: UINavigationController) {
        self.window = window
        self.navigationController = navigationController
        super.init()
        self.window.rootViewController = navigationController
        self.window.makeKeyAndVisible()
        self.setupNavigationBarStyle()
    }
    
    func start(navigation: Any) {
        let productCoordinator = AppConfiguration.shared.dependencyInjector.getProductCoordinator(navigationcontroller: self.navigationController)
        productCoordinator.start(navigation: navigation)
    }
}

extension AppFlowCoordinator {
    
    func setupNavigationBarStyle() {
        self.navigationController.setTransparentbackground()
        self.navigationController.setAppThemeStyle()
    }
}
